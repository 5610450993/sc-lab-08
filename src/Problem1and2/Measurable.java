package Problem1and2;

public interface Measurable {
	double getMeasurable();
	String toString();
}
