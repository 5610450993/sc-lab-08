package Problem1and2;

public class BankAccount implements Measurable{
	String name ;
	double balance ;
	
	BankAccount(String name, double balance){
		this.name = name;
		this.balance = balance;
	}
	
	@Override
	public double getMeasurable() {
		return balance ;
	}
	
	public String toString(){
		return name + " , " + balance ;
	}
	
}
