package Problem1and2;

public class Test {

	public static void main(String[] args) {
		Test t = new Test();
		t.testPerson();
		t.testMin();

	}
	
	public void testPerson(){
		Measurable[] person = new Measurable[5];
		person[0] = new Person("A", 150);
		person[1] = new Person("B", 160);
		person[2] = new Person("C", 170);
		person[3] = new Person("C", 180);
		person[4] = new Person("C", 190);
		System.out.println("<--Test_1-->\n" + Data.average(person));
	}
	
	public void testMin(){
		Measurable m1, m2, min;
		m1 = new Person("A", 150);
		m2 = new Person("B", 160);
		System.out.println("\n<--Test_2-->\n" + Data.min(m1,m2)+"\n");
		
		Measurable[] pbc = new Measurable[3];
		pbc[0] = Data.min(m1,m2);
		m1 = new BankAccount("A", 1000);
		m2 = new BankAccount("B", 2000);
		pbc[1] = Data.min(m1,m2);
		m1 = new Country("Thai", 1234567);
		m2 = new Country("Laos", 2345678);
		pbc[2] = Data.min(m1,m2);
		System.out.println(Data.average(pbc));
	}

}
