package Problem1and2;

public class Country implements Measurable{
	String country ;
	double area ;
	
	Country(String country, double area){
		this.country = country;
		this.area = area;
	}
	
	@Override
	public double getMeasurable() {
		return area ;
	}
	
	public String toString(){
		return country + " , " + area ;
	}

	
	
}
