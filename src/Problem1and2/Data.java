package Problem1and2;

public class Data {
	
	public static double average(Measurable[] objects){
		double sum = 0 ;
		double n = objects.length;
		for(Measurable m : objects){
			sum += m.getMeasurable();
		}
		if(n > 0) return sum/n ;
		return 0 ;
	}
	
	public static Measurable min(Measurable m1, Measurable m2){
		if(m1.getMeasurable() < m2.getMeasurable()) return m1 ;
		return m2 ;
	}
}
