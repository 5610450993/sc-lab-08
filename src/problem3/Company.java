package problem3;


public class Company implements Taxable{
	private String company ;
	private double income ;
	private double expense ;
	
	Company(String company, double income, double expense){
		this.company = company;
		this.income = income;
		this.expense = expense;
	}
	
	@Override
	public double getTax() {
		return (income-expense)*0.3;
	}
}
