package problem3;

import java.util.ArrayList;

public class Test {

	public static void main(String[] args) {
		Test t = new Test();
		t.testTax();
	}
	
	public void testTax(){
		ArrayList<Taxable> taxPer = new ArrayList<Taxable>();
		taxPer.add(new Person("A", 500000));
		taxPer.add(new Person("B", 250000));
		System.out.println("Tax Person : " + TaxCalculator.sum(taxPer) + "\n");
		
		ArrayList<Taxable> taxCom = new ArrayList<Taxable>();
		taxCom.add(new Company("A", 40000 , 4000));
		taxCom.add(new Company("B", 100000 , 10000));
		System.out.println("Tax Company : " + TaxCalculator.sum(taxCom) + "\n");
		
		ArrayList<Taxable> taxPro = new ArrayList<Taxable>();
		taxPro.add(new Product("A", 50000));
		taxPro.add(new Product("B", 5));
		System.out.println("Tax Product : " + TaxCalculator.sum(taxPro) + "\n");
		
		ArrayList<Taxable> taxAll = new ArrayList<Taxable>();
		taxAll.add(new Person("A", 500000));
		taxAll.add(new Company("A", 40000 , 4000));
		taxAll.add(new Product("A", 50000));
		System.out.println("Tax All : " + TaxCalculator.sum(taxAll) + "\n");
	}

}
