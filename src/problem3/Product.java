package problem3;

public class Product implements Taxable{
	private String product ;
	private double price ;

	Product(String product, double price){
		this.product = product;
		this.price = price;
	}

	@Override
	public double getTax() {
		return price*0.07 ;
	}
	
}
