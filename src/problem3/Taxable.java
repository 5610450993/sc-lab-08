package problem3;

public interface Taxable {
	double getTax();
}
