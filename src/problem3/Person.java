package problem3;

public class Person implements Taxable{
	private String name ;
	private double income ;
	
	public Person(String name, double income){
		this.name = name;
		this.income = income;
	}

	@Override
	public double getTax() {
		if(income < 300001) return income*0.05 ;
		return (300000*0.05)+(income-300000)*0.1;
	}
}
