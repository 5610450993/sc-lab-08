package problem3;

import java.util.ArrayList;

public class TaxCalculator {

	public static double sum(ArrayList<Taxable> taxList){
		double sum = 0 ;
		for(Taxable t : taxList){
			sum += t.getTax();
		}
		if(taxList.size() > 0) return sum ;
		return 0 ;
	}
}
